package fileutils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class FileScanner {

    private void scanFiles(Path dirPath, List<Path> files) throws IOException {
        if (Files.exists(dirPath)){
            Files.list(dirPath).forEach(path -> {
                if (Files.isDirectory(path)) {
                    try {
                        scanFiles(path, files);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    files.add(path);
                }
            });
        }
    }

    public List<Path> scanFiles(Path dirPath) throws IOException {
        List<Path> files = new ArrayList<>();
        scanFiles(dirPath, files);
        return files;
    }


}
