package fileutils;

import javax.inject.Named;
import javax.inject.Singleton;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@Named
@Singleton
@SuppressWarnings("unused")
public class SHA256FileHashCalculator extends FileHashCalculator {

    @Override
    public String calculateFromFiles(List<Path> files) throws IOException {
        byte[] checksum = new byte[32];
        if (files == null || files.size() == 0) return "";
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            for (Path file : files) {
                try (FileInputStream fis = new FileInputStream(file.toFile())) {
                    byte[] byteArray = new byte[8192];
                    int bytesCount;

                    while ((bytesCount = fis.read(byteArray)) != -1) {
                        digest.update(byteArray, 0, bytesCount);
                    }
                }
            }
            checksum = digest.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return convertToString(checksum);
    }

    @Override
    public String calculateFromStrings(List<String> hashCodes) {
        byte[] checksum = new byte[32];
        if (hashCodes == null || hashCodes.size() == 0) return "";
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            for (String hash : hashCodes) {
                int bytesCount = hash.getBytes().length;
                digest.update(hash.getBytes(), 0, bytesCount);
            }
            checksum = digest.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return convertToString(checksum);
    }

}
