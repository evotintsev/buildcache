package fileutils;

import java.nio.file.Path;
import java.util.List;

public abstract class FileHashCalculator {

    public abstract String calculateFromFiles(List<Path> files) throws Exception;
    public abstract String calculateFromStrings(List<String> hashCodes) throws Exception;


    protected String convertToString(byte[] checksum) {
        StringBuilder sb = new StringBuilder();

        for (byte aByte : checksum) {
            sb.append(Integer
                    .toString((aByte & 0xff) + 0x100, 16)
                    .substring(1));
        }
        return sb.toString();
    }
}
