package fileutils;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import javax.management.modelmbean.XMLParseException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class XmlConfigParser {

    public Document parse(URL url) throws DocumentException {
        SAXReader reader = new SAXReader();
        return reader.read(url);
    }

    //write exception for it
    public Map<String, String> parseConfigProperties(Document document) throws XMLParseException {
        Element root = document.getRootElement();
        Map<String, String> result = new HashMap<>();
        if (!root.getName().equals("config"))
            throw new XMLParseException("config element not found");
        for (Iterator<Element> it = root.elementIterator(); it.hasNext();) {
            Element element = it.next();
            if (element.getName().equals("remote-cache") || element.getName().equals("local-cache")){
                for (Iterator<Element> childIterator = element.elementIterator(); childIterator.hasNext();) {
                    Element childElement = childIterator.next();
                    result.put(childElement.getName(), childElement.getStringValue());
                }
            }
        }

        return result;
    }

}
