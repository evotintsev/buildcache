package buildCache;

import fileutils.FileHashCalculator;
import fileutils.FileScanner;
import org.apache.maven.project.MavenProject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Named
@Singleton
public class ProjectsScanner {

    private final Logger LOGGER = LoggerFactory.getLogger(ProjectsScanner.class);

    private final BuildCache cache;
    private final FileHashCalculator fileHashCalculator;

    @Inject
    public ProjectsScanner(BuildCache cache, FileHashCalculator fileHashCalculator) {
        this.cache = cache;
        this.fileHashCalculator = fileHashCalculator;
    }

    public void scanProjects(List<MavenProject> projects) throws Exception {
        for (MavenProject project : projects) {
            if (project.getPackaging().equals("pom"))
                cache.addProject(new CacheableMavenProject(project, "", CacheStatus.NOT_CACHED, false));
            else {
                Map<String, MavenProject> dependenciesProjects = project.getProjectReferences();
                List<String> hashCodes = new ArrayList<>();
                for (MavenProject dependency : dependenciesProjects.values()) {
                    CacheableMavenProject cacheableMavenProject = cache.findCacheableProject(dependency);
                    if (cacheableMavenProject != null) {
                        hashCodes.add(cacheableMavenProject.getHash());
                    }
                }

                String hash = fileHashCalculator.calculateFromFiles(prepareFiles(project));
                hashCodes.add(hash);
                hash = hashCodes.size() > 1 ? fileHashCalculator.calculateFromStrings(hashCodes) : hash;


                if (hash.isEmpty()) {
                    LOGGER.info("[BUILD CACHE] hash is empty for project {}", project.getName());
                    cache.addProject(new CacheableMavenProject(project, hash, CacheStatus.NOT_CACHED,
                            false));
                } else {
                    CacheStatus status = cache.contains(hash);
                    if (status != CacheStatus.NOT_CACHED) {
                        LOGGER.info("[BUILD CACHE] cached artifact for project {} found hash: {}", project.getName(), hash);
                        cache.addProject(new CacheableMavenProject(project, hash, status, false));
                    } else {
                        cache.addProject(new CacheableMavenProject(project, hash, CacheStatus.NOT_CACHED,
                                true));
                        LOGGER.info("[BUILD CACHE] artifact for project {} will be cached hash: {}", project.getName(), hash);
                    }
                }

            }
        }
    }

    private List<Path> prepareFiles(MavenProject project) throws IOException {
        FileScanner fileScanner = new FileScanner();
        List<Path> files = new ArrayList<>();
        for (String root : project.getCompileSourceRoots()) {
            files.addAll(fileScanner.scanFiles(Path.of(root)));
        }

        for (String root : project.getTestCompileSourceRoots()) {
            files.addAll(fileScanner.scanFiles(Path.of(root)));
        }
        //add pom
        files.add(project.getFile().toPath());
        //add parent pom
        if (project.getParent() != null)
            files.add(project.getParent().getFile().toPath());
        return files;
    }
}
