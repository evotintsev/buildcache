package buildCache;

import org.apache.maven.execution.MojoExecutionEvent;
import org.apache.maven.execution.MojoExecutionListener;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.MavenProject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

@Named
@Singleton
@SuppressWarnings("unused")
public class BuildCacheMojoExecutionListener implements MojoExecutionListener {

    private final Logger LOGGER = LoggerFactory.getLogger(BuildCacheMojoExecutionListener.class);

    private final BuildCache cache;

    @Inject
    public BuildCacheMojoExecutionListener(BuildCache cache) {
        this.cache = cache;
    }

    @Override
    public void beforeMojoExecution(MojoExecutionEvent mojoExecutionEvent) throws MojoExecutionException {
        String phase = mojoExecutionEvent.getExecution().getLifecyclePhase();
        if (cache.isUseCache()) {
            try {
                MavenProject project = mojoExecutionEvent.getProject();
                switch (phase) {
                    case "process-resources":
                        if (cache.isProjectCashed(project)) {
                            if (cache.unpackToTarget(project)) {
                                LOGGER.info("[BUILD CACHE] project {} saved to target dir", project);
                            }
                        }
                        break;
                    case "test":
                        if (!cache.isProjectCashed(project) && cache.isProjectNeedCache(project)) {
                            if (cache.storeToCache(project))
                                LOGGER.info("[BUILD CACHE] artifacts for project {} stored to cache", project.getName());
                            else LOGGER.info("[BUILD CACHE] error while uploading project {} to cache", project.getName());
                        }
                }
            } catch (Exception e) {
                e.printStackTrace();
                throw new MojoExecutionException(e.getMessage());
            }
        }
    }

    @Override
    public void afterMojoExecutionSuccess(MojoExecutionEvent mojoExecutionEvent) throws MojoExecutionException {

    }

    @Override
    public void afterExecutionFailure(MojoExecutionEvent mojoExecutionEvent) {

    }
}
