package buildCache;

import org.apache.maven.project.MavenProject;

import java.io.File;

//todo: make immutable?
public class CacheableMavenProject {
    private MavenProject mavenProject;
    private String hash;
    private CacheStatus status;
    private boolean needCache;
    private File cache;

    public CacheableMavenProject(MavenProject mavenProject, String hash, CacheStatus status, boolean needCache) {
        this.mavenProject = mavenProject;
        this.hash = hash;
        this.status = status;
        this.needCache = needCache;
    }

    public MavenProject getMavenProject() {
        return mavenProject;
    }

    public String getHash() {
        return hash;
    }

    public CacheStatus getStatus() {
        return status;
    }

    public boolean isNeedCache() {
        return needCache;
    }

    public void setMavenProject(MavenProject mavenProject) {
        this.mavenProject = mavenProject;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public void setStatus(CacheStatus status) {
        this.status = status;
    }

    public void setNeedCache(boolean needCache) {
        this.needCache = needCache;
    }

    public File getCache() {
        return cache;
    }

    public void setCache(File cache) {
        this.cache = cache;
    }
}
