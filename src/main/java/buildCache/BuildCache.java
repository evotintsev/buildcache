package buildCache;

import fileutils.FilePackager;
import io.minio.errors.ErrorResponseException;
import io.minio.errors.ServerException;
import org.apache.maven.project.MavenProject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import services.CacheService;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.net.ssl.SSLException;
import java.io.File;
import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Named
@Singleton
public class BuildCache {

    @Inject
    @Named("localCacheService")
    private CacheService localCacheService;

    @Inject
    @Named("remoteCacheService")
    private CacheService remoteCacheService;

    private boolean useCache = false;
    private boolean offlineMode = false;
    private final Logger LOGGER = LoggerFactory.getLogger(BuildCache.class);
    private final Map<String, CacheableMavenProject> projects = new HashMap<>();

    public CacheStatus contains(String hash) throws Exception {
        boolean local = false;
        boolean remote = false;
        try {
            local = localCacheService.contains(hash);

            if (!offlineMode) {
                remote = remoteCacheService.contains(hash);
            }
        } catch (SocketException | ServerException | ErrorResponseException | SSLException e) {
            LOGGER.warn(e.getMessage());
            LOGGER.warn("[BUILD CACHE] Disable remote caching");
            offlineMode = true;
        }
        if (local && remote) return CacheStatus.LOCAL_AND_REMOTE_CACHED;
        else if (remote) return CacheStatus.REMOTE_CACHED;
        else if (local) return CacheStatus.LOCAL_CACHED;
        else return CacheStatus.NOT_CACHED;
    }

    public void addProject(CacheableMavenProject project) {
        projects.put(project.getMavenProject().getArtifactId(), project);
    }

    //todo: think about exception
    public boolean isProjectCashed(MavenProject project) {
        Optional<CacheableMavenProject> optional = Optional.ofNullable(projects.get(project.getArtifactId()));
        return optional.map(mvnProject -> mvnProject.getStatus() != CacheStatus.NOT_CACHED).orElse(false);
    }

    public boolean isProjectNeedCache(MavenProject project) {
        Optional<CacheableMavenProject> optional = Optional.ofNullable(projects.get(project.getArtifactId()));
        return optional.map(CacheableMavenProject::isNeedCache).orElse(false);
    }

    //todo: think about exception
    public boolean storeToCache(MavenProject project) throws Exception {
        CacheableMavenProject cacheableMavenProject = findCacheableProject(project);
        if (cacheableMavenProject != null) {
            String hash = cacheableMavenProject.getHash();
            File artifactFiles = new File(cacheableMavenProject.getMavenProject().getBuild().getDirectory());
            if (artifactFiles.listFiles() != null) {
                File zipDir = FilePackager.zipDirectory(artifactFiles, hash);
                try {
                    localCacheService.put(zipDir, hash);
                    if (!offlineMode) {
                        remoteCacheService.put(zipDir, hash);
                    }
                } catch (ConnectException e) {
                    LOGGER.info("[BUILD CACHE] cannot connect to remote cache");
                    LOGGER.info("[BUILD CACHE] error message: {}", e.getMessage());
                } catch (Exception e) {
                    e.printStackTrace();
                    throw e;
                } finally {
                    zipDir.delete();
                    cacheableMavenProject.setNeedCache(false);
                }
                return true;
            } else return false;
        } else return false;

    }

    public boolean getFromCache(MavenProject project) throws Exception {
        CacheableMavenProject cacheableMavenProject = findCacheableProject(project);
        File cachedFile = null;
        if (cacheableMavenProject != null) {
            CacheStatus status = cacheableMavenProject.getStatus();
            try {
                String hash = cacheableMavenProject.getHash();
                switch (status) {
                    case LOCAL_CACHED:
                        cachedFile = localCacheService.get(hash);
                        if (!offlineMode)
                            remoteCacheService.put(cachedFile, hash);
                        break;
                    case REMOTE_CACHED:
                        if (!offlineMode) {
                            cachedFile = remoteCacheService.get(hash);
                            localCacheService.put(cachedFile, hash);
                        } else return false;
                        break;
                    case LOCAL_AND_REMOTE_CACHED:
                        cachedFile = localCacheService.get(hash);
                        break;
                    default:
                        return false;
                }
            } catch (ConnectException | ServerException e) {
                LOGGER.info("[BUILD CACHE] cannot connect to remote cache");
                LOGGER.info("[BUILD CACHE] error message: {}", e.getMessage());
                offlineMode = true;
            } catch (Exception e) {
                e.printStackTrace();
                throw e;
            }
            if (cachedFile != null) {
                cacheableMavenProject.setCache(cachedFile);
                return true;
            } else return false;
        } else return false;
    }

    public boolean unpackToTarget(MavenProject mavenProject) throws IOException {
        CacheableMavenProject cacheableMavenProject = findCacheableProject(mavenProject);
        File cache = cacheableMavenProject.getCache();
        if (cache != null) {
            FilePackager.unzipArchive(cache, mavenProject.getBasedir());
            if (cacheableMavenProject.getStatus() == CacheStatus.REMOTE_CACHED) {
                cache.delete();
            }
            cacheableMavenProject.setCache(null);
            return true;
        } else return false;
    }

    public CacheableMavenProject findCacheableProject(MavenProject project) {
        return projects.get(project.getArtifactId());
    }

    public boolean isUseCache() {
        return useCache;
    }

    public void setUseCache(boolean useCache) {
        this.useCache = useCache;
    }

    public void setOfflineMode(boolean offlineMode) {
        this.offlineMode = offlineMode;
    }
}
