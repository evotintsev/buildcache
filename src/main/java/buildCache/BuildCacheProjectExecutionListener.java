package buildCache;

import org.apache.maven.execution.ProjectExecutionEvent;
import org.apache.maven.execution.ProjectExecutionListener;
import org.apache.maven.lifecycle.LifecycleExecutionException;
import org.apache.maven.project.MavenProject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.List;

@Named
@Singleton
@SuppressWarnings("unused")
public class BuildCacheProjectExecutionListener implements ProjectExecutionListener {

    private final Logger LOGGER = LoggerFactory.getLogger(BuildCacheProjectExecutionListener.class);

    private final BuildCache cache;
    private final ExecutionPlanChanger executionPlanChanger;

    @Inject
    public BuildCacheProjectExecutionListener(BuildCache cache, ExecutionPlanChanger executionPlanChanger) {
        this.cache = cache;
        this.executionPlanChanger = executionPlanChanger;
    }

    @Override
    public void beforeProjectExecution(ProjectExecutionEvent projectExecutionEvent) throws LifecycleExecutionException {
    }

    @Override
    public void beforeProjectLifecycleExecution(ProjectExecutionEvent projectExecutionEvent)
            throws LifecycleExecutionException {
        MavenProject project = projectExecutionEvent.getProject();
        if (cache.isUseCache() && cache.isProjectCashed(project)) {
            try {
                if (cache.getFromCache(project)) {
                    LOGGER.info("[BUILD CACHE] project {} loaded from cache", project);
                    executionPlanChanger.skipPhases(projectExecutionEvent.getExecutionPlan(),
                            List.of("compile", "test-compile"));
                } else {
                    LOGGER.info("[BUILD CACHE] cannot load project {} from cache", project);
                }
            } catch (Exception e) {
                throw new LifecycleExecutionException(e);
            }
        }
    }


    @Override
    public void afterProjectExecutionSuccess(ProjectExecutionEvent projectExecutionEvent) throws LifecycleExecutionException {
        MavenProject project = projectExecutionEvent.getProject();
        try {
            if (!cache.isProjectCashed(project) && cache.isProjectNeedCache(project)) {
                if (cache.storeToCache(project))
                    LOGGER.info("[BUILD CACHE] artifacts for project {} stored to cache", project.getName());
                else LOGGER.info("[BUILD CACHE] error while uploading project {} to cache", project.getName());
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new LifecycleExecutionException(e.getMessage());
        }

    }

    @Override
    public void afterProjectExecutionFailure(ProjectExecutionEvent projectExecutionEvent) {

    }
}
