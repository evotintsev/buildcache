package buildCache;

public enum CacheStatus {
    NOT_CACHED, LOCAL_CACHED, REMOTE_CACHED, LOCAL_AND_REMOTE_CACHED
}
