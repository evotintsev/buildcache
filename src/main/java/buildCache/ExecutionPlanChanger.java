package buildCache;

import org.apache.maven.plugin.MojoExecution;

import javax.inject.Named;
import java.util.Iterator;
import java.util.List;

//todo: maybe make it abstract
@Named
public class ExecutionPlanChanger {

    private final List<String> defaultPlugins = List.of(
            "maven-resources-plugin",
            "maven-compiler-plugin",
            "maven-surefire-plugin",
            "maven-jar-plugin");

    public void skipDefaultPlugins(List<MojoExecution> executionPlan) {
        executionPlan.removeIf(mojoExecution -> defaultPlugins.contains(mojoExecution.getArtifactId()));
    }

    public void skipAllPlugins(List<MojoExecution> executionPlan, List<String> pluginsNotToSkip) {
        executionPlan.removeIf(mojoExecution -> !mojoExecution.getLifecyclePhase().equals("install")
                && !mojoExecution.getLifecyclePhase().equals("clean")
                && !pluginsNotToSkip.contains(mojoExecution.getArtifactId()));
    }

    public void skipPhases(List<MojoExecution> executionPlan, List<String> phases){
        executionPlan.removeIf(mojoExecution -> phases.contains(mojoExecution.getLifecyclePhase()));
    }

    public void skipPluginsBeforePhase(List<MojoExecution> executionPlan, String phase) {
        Iterator<MojoExecution> iterator = executionPlan.iterator();
        MojoExecution mojoExecution = iterator.next();
        while (iterator.hasNext() && !mojoExecution.getLifecyclePhase().equals(phase)) {
            iterator.remove();
            mojoExecution = iterator.next();
        }
    }
}
