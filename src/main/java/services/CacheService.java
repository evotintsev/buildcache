package services;

import java.io.File;

public interface CacheService {
    void put(File file, String hash) throws Exception;
    File get(String hash) throws Exception;
    boolean contains(String hash) throws Exception;
}
