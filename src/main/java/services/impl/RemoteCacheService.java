package services.impl;

import config.CacheConfig;
import io.minio.*;
import io.minio.errors.*;
import services.CacheService;

import javax.inject.Named;
import javax.inject.Singleton;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ConnectException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

//todo: think about bug whith cached but has in remote cache
@Named("remoteCacheService")
@Singleton
@SuppressWarnings("unused")
public class RemoteCacheService implements CacheService {

    private final String bucketName = "build-cache";
    private MinioClient minioClient;

    public RemoteCacheService() {
        try {
            String trustStore = CacheConfig.getProperty("trust-store-path");
            String trustStorePassword = CacheConfig.getProperty("trust-store-password");
            if (trustStore != null && trustStorePassword != null) {
                System.setProperty("javax.net.ssl.trustStore", trustStore);
                System.setProperty("javax.net.ssl.trustStorePassword", trustStorePassword);
            }
            minioClient = MinioClient.builder()
                    .endpoint(CacheConfig.getProperty("url"), Integer.parseInt(CacheConfig.getProperty("port")),
                            Boolean.parseBoolean(CacheConfig.getProperty("use-tls")))
                    .credentials(CacheConfig.getProperty("username"), CacheConfig.getProperty("password"))
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
            minioClient = null;
        }
    }

    @Override
    public void put(File file, String hash) throws MinioException, IOException,
            NoSuchAlgorithmException, InvalidKeyException {
        if (minioClient != null) {
            createBucketIfNotExists(bucketName);
            minioClient.uploadObject(
                    UploadObjectArgs.builder()
                            .bucket(bucketName)
                            .object(hash)
                            .filename(file.getAbsolutePath())
                            .build());
        }
    }

    @Override
    public File get(String hash) throws Exception {
        if (minioClient != null) {
            File cachedArtifact = new File(hash);
            boolean found = minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build());
            if (!found) throw new FileNotFoundException("bucket not found");
            minioClient.downloadObject(
                    DownloadObjectArgs.builder()
                            .bucket(bucketName)
                            .object(hash)
                            .filename(cachedArtifact.getAbsolutePath())
                            .build());
            return cachedArtifact;
        } else throw new ConnectException("Illegal server arguments");
    }

    //todo: think about exceptions
    @Override
    public boolean contains(String hash) throws Exception {
        if (minioClient != null) {
            boolean found = minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build());
            if (!found) return false;
            try {
                minioClient.statObject(
                        StatObjectArgs.builder().bucket(bucketName).object(hash).build());
            } catch (ErrorResponseException e) {
                String code = e.errorResponse().code();
                if (code.equals("NoSuchKey")) {
                    return false;
                } else throw e;
            }
            return true;
        } else throw new ConnectException("Illegal server arguments");
    }

    private boolean createBucketIfNotExists(String bucketName) throws ServerException, InsufficientDataException, ErrorResponseException, IOException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
        if (minioClient != null) {
            boolean found = minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build());
            if (!found) {
                minioClient.makeBucket(MakeBucketArgs.builder().bucket(bucketName).build());
            }
            return !found;
        } else throw new ConnectException("Illegal server arguments");
    }
}
