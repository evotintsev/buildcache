package services.impl;

import config.CacheConfig;
import org.apache.commons.io.FileUtils;
import services.CacheService;

import javax.inject.Named;
import javax.inject.Singleton;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@Named("localCacheService")
@Singleton
@SuppressWarnings("unused")
public class LocalCacheService implements CacheService {
    private final String CACHE_DIR = CacheConfig.getProperty("path");

    @Override
    public void put(File file, String hash) throws IOException {
        if (file == null) return;
            File cacheDir = new File(Path.of(CACHE_DIR).toUri());
            if (!cacheDir.exists()) {
                Files.createDirectory(cacheDir.toPath());
            }
            File cachedFile = new File(Path.of(cacheDir.getAbsolutePath(), hash).toUri());
            FileUtils.copyFile(file, cachedFile);

    }

    @Override
    public File get(String hash) throws FileNotFoundException {
        File cachedFile = new File (Path.of(CACHE_DIR, hash).toUri());
        if (!cachedFile.exists()) throw new FileNotFoundException();

        return cachedFile;
    }

    @Override
    public boolean contains(String hash) {
        File dir = new File(Path.of(CACHE_DIR, hash).toUri());
        return dir.exists();
    }
}
