package config;

import fileutils.XmlConfigParser;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.management.modelmbean.XMLParseException;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

public class CacheConfig {
    private static final String MULTIMODULE_PROJECT_DIRECTORY = "maven.multiModuleProjectDirectory";
    private static final Path CONFIG_PATH = Path.of(".mvn", "cache-config.xml");
    private static Map<String, String> properties = new HashMap<>();
    private final static Logger LOGGER = LoggerFactory.getLogger(CacheConfig.class);

    static {
        try {
            Path configFullPath = Path.of(System.getProperty(MULTIMODULE_PROJECT_DIRECTORY), CONFIG_PATH.toString());
            XmlConfigParser configParser = new XmlConfigParser();
            Document document = configParser.parse(configFullPath.toUri().toURL());
            properties = configParser.parseConfigProperties(document);
        } catch (DocumentException e) {
            LOGGER.error(e.getMessage());
        } catch (XMLParseException e) {
            LOGGER.error("[BUILD CACHE] Error while parsing config file");
        } catch (MalformedURLException e) {
            LOGGER.error("[BUILD CACHE] Wrong config file URL");
        }

    }

    public static String getProperty(String name){
        return properties.get(name);
    }
}
