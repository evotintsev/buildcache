package participants;

import buildCache.BuildCache;
import buildCache.ProjectsScanner;
import config.CacheConfig;
import org.apache.maven.AbstractMavenLifecycleParticipant;
import org.apache.maven.MavenExecutionException;
import org.apache.maven.execution.MavenSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.List;

@Named
@Singleton
public class BuildCacheMavenLifecycleParticipant extends AbstractMavenLifecycleParticipant {

    private final ProjectsScanner projectsScanner;
    private final BuildCache buildCache;
    private final Logger LOGGER = LoggerFactory.getLogger(BuildCacheMavenLifecycleParticipant.class);
    private final List<String> supportedGoals = List.of("compile", "test-compile", "test", "package", "verify", "install", "deploy");

    @Inject
    public BuildCacheMavenLifecycleParticipant(ProjectsScanner projectsScanner, BuildCache buildCache) {
        this.projectsScanner = projectsScanner;
        this.buildCache = buildCache;
    }

    @Override
    public void afterProjectsRead(MavenSession session) throws MavenExecutionException {
        List<String> goals = session.getRequest().getGoals();
        if (containsSupportedGoals(goals)){
            buildCache.setUseCache(true);
            try {
                if (CacheConfig.getProperty("path") != null) {
                    if (CacheConfig.getProperty("url") == null || CacheConfig.getProperty("username") == null
                            || CacheConfig.getProperty("password") == null) {
                        LOGGER.warn("[BUILD CACHE] Remote cache parameters not specified");
                        LOGGER.warn("[BUILD CACHE] Disable remote caching");
                        buildCache.setOfflineMode(true);
                    }
                    projectsScanner.scanProjects(session.getProjects());
                } else {
                    LOGGER.warn("[BUILD CACHE] Local cache path not specified");
                    LOGGER.warn("[BUILD CACHE] Disable caching");
                }
            } catch (Exception e) {
                throw new MavenExecutionException(e.getMessage(), e);
            }
        } else {
            buildCache.setUseCache(false);
        }

    }

    private boolean containsSupportedGoals(List<String> goals) {
        for (String goal : supportedGoals) {
            if (goals.contains(goal)) return true;
        }
        return false;
    }
}
