import fileutils.FileHashCalculator;
import fileutils.FileScanner;
import fileutils.SHA256FileHashCalculator;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.junit.jupiter.api.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class TestBuild {
    private final static String WORKING_DIR = "src/test/testSources/mediumJavaMultiProject";
    private final static List<String> commands = List.of("clean", "test-compile", "-DskipTests");
    private final String[] HEADERS = {"project", "phaseCount"};
    private final String TEXT = "<!---->";
    private final List<String> PROJECTS_TO_CHANGE = List.of("project0", "project4");
    private final List<String> PROJECTS_SHOULD_CHANGED = List.of("project0", "project4", "project3",
            "project12", "project7");

    @BeforeEach
    void setUp() throws IOException {
        cleanCache();
    }

    @AfterAll
    static void tearDown() throws IOException {
        cleanCache();
    }

    @Test
    void testBuild() {
        try {
            int exitCode = startBuild();
            Assertions.assertEquals(0, exitCode);
            Map<String, String> expectedHashCodes = scanProject(WORKING_DIR);
            exitCode = startBuild();
            Assertions.assertEquals(0, exitCode);
            Map<String, String> actualHashCodes = scanProject(WORKING_DIR);
            expectedHashCodes.forEach((project, hash) -> {
                Assertions.assertEquals(hash, actualHashCodes.get(project), "hash code of project: "
                        + project + " not equals to first build");
            });
            Iterable<CSVRecord> records = readCsvResult(WORKING_DIR);
            for (CSVRecord record : records) {
                String count = record.get("phaseCount");
                Assertions.assertEquals(0, Integer.parseInt(count),
                        "compile phases haven't been skipped for " + record.get("project"));
            }
        } catch (IOException | InterruptedException e) {
            Assertions.fail(e.getMessage());
        }
    }

    @Test
    void testIncrementalBuild() {
        try {
            int exitCode = startBuild();
            Assertions.assertEquals(0, exitCode);
            changeProjects(PROJECTS_TO_CHANGE, WORKING_DIR);
            exitCode = startBuild();
            Assertions.assertEquals(0, exitCode);
            revertChangeProject(PROJECTS_TO_CHANGE, WORKING_DIR);
            Iterable<CSVRecord> records = readCsvResult(WORKING_DIR);
            for (CSVRecord record : records) {
                String count = record.get("phaseCount");
                String project = record.get("project");
                if (PROJECTS_SHOULD_CHANGED.contains(project)) {
                    Assertions.assertEquals(2, Integer.parseInt(count),
                            "compile phases have been skipped for " + project);
                } else {
                    Assertions.assertEquals(0, Integer.parseInt(count),
                            "compile phases haven't been skipped for " + project);
                }
            }
        } catch (IOException | InterruptedException e) {
            Assertions.fail(e.getMessage());
        }

    }

    private static List<String> getOsSpecificArgs(List<String> commandAndArgs) {
        boolean isWindows = System.getProperty("os.name").toLowerCase().startsWith("windows");
        List<String> args = new ArrayList<>();
        if (isWindows) {
            args.add("cmd");
            args.add("/c");
        } else {
            args.add("sh");
            args.add("-c");
        }
        args.add(String.join(" ", commandAndArgs));
        return args;
    }

    private int startBuild() throws IOException, InterruptedException {
        ProcessBuilder processBuilder = new ProcessBuilder();
        int exitCode;
        List<String> args = new ArrayList<>();
        File workingDir = new File(WORKING_DIR);
        processBuilder.directory(workingDir);
        args = getOsSpecificArgs(args);
        boolean isWindows = System.getProperty("os.name").toLowerCase().startsWith("windows");
        List<String> mvnw = new ArrayList<>(List.of(isWindows ? "mvnw" : "./mvnw"));
        args.addAll(mvnw);
        args.addAll(commands);
        processBuilder.command(args);
        processBuilder.redirectError(ProcessBuilder.Redirect.INHERIT)
                .redirectOutput(ProcessBuilder.Redirect.INHERIT)
                .redirectInput(ProcessBuilder.Redirect.INHERIT);

        Process process = processBuilder.start();
        exitCode = process.waitFor();

        return exitCode;
    }

    private static Map<String, String> scanProject(String path) {
        Map<String, String> hashCodes = new HashMap<>();
        scanProjectRecursive(path, hashCodes);
        return hashCodes;
    }

    private static void cleanCache() throws IOException {
        if (Files.exists(Path.of(WORKING_DIR, "cache"))) {
            Files.walk(Path.of(WORKING_DIR, "cache"))
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
        }
    }

    private static void scanProjectRecursive(String path, Map<String, String> hashCodes) {
        FileScanner scanner = new FileScanner();
        try {
            Files.list(Path.of(path)).forEach(child -> {
                if (Files.isDirectory(child)) {
                    File target = new File(Path.of(child.toString(), "target").toString());
                    if (target.exists()) {
                        try {
                            FileHashCalculator fileHashCalculator = new SHA256FileHashCalculator();
                            List<Path> compileSources = scanner.scanFiles(target.toPath());
                            String hash = fileHashCalculator.calculateFromFiles(compileSources);
                            hashCodes.put(child.getFileName().toString(), hash);
                        } catch (Exception e) {
                            Assertions.fail(e.getMessage());
                        }
                    } else {
                        scanProjectRecursive(child.toString(), hashCodes);
                    }
                }
            });
        } catch (IOException e) {
            Assertions.fail(e.getMessage());
        }
    }

    //todo: won't work with other projects
    private void changeProjects(List<String> projects, String projectPath) {
        try {
            Files.list(Path.of(projectPath)).forEach(child -> {
                if (Files.isDirectory(child)) {
                    if (projects.contains(child.getFileName().toString())) {
                        File pom = new File(Path.of(child.toString(), "pom.xml").toString());
                        if (pom.exists()) {
                            try (FileWriter fileWriter = new FileWriter(pom, true)) {
                                fileWriter.write(TEXT);
                            } catch (IOException e) {
                                Assertions.fail(e.getMessage());
                            }
                        }
                    }

                }
            });
        } catch (IOException e) {
            Assertions.fail(e.getMessage());
        }
    }

    private boolean revertChangeProject(List<String> projects, String projectPath) {
        AtomicInteger changesCount = new AtomicInteger();
        try {
            Files.list(Path.of(projectPath)).forEach(child -> {
                if (Files.isDirectory(child)) {
                    if (projects.contains(child.getFileName().toString())) {
                        File originalPom = new File(Path.of(child.toString(), "pom.xml").toString());
                        if (originalPom.exists()) {
                            File newPom = new File(Path.of(child.toString(), "newPom.xml").toString());
                            try (BufferedReader reader = new BufferedReader(new FileReader(originalPom));
                                 BufferedWriter writer = new BufferedWriter(new FileWriter(newPom))
                            ) {
                                String currentLine;
                                while ((currentLine = reader.readLine()) != null) {
                                    String trimmedLine = currentLine.trim();
                                    if (trimmedLine.endsWith(TEXT)) {
                                        writer.write(trimmedLine.replace(TEXT, ""));
                                    } else writer.write(currentLine + System.getProperty("line.separator"));
                                }

                            } catch (IOException e) {
                                Assertions.fail(e.getMessage());
                            }
                            boolean delete = originalPom.delete();
                            boolean rename = newPom.renameTo(originalPom);
                            if (delete && rename) changesCount.getAndIncrement();
                        }
                    }
                }
            });
        } catch (IOException e) {
            Assertions.fail(e.getMessage());
        }
        return changesCount.get() == projects.size();
    }

    //todo: remove deprecated methods
    private Iterable<CSVRecord> readCsvResult(String path) throws IOException {
        Reader in = new FileReader(Path.of(path, "result.csv").toString());
        return CSVFormat.DEFAULT
                .withHeader(HEADERS)
                .withFirstRecordAsHeader()
                .parse(in);
    }
}
