import fileutils.FileHashCalculator;
import fileutils.FileScanner;
import fileutils.SHA256FileHashCalculator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;

public class FileHashCalculatorTest {
    public FileHashCalculator fileHashCalculator = new SHA256FileHashCalculator();
    public String TEST_SOURCES_DIR = "src/test/testSources";

    @ParameterizedTest
    @CsvFileSource(resources = "/files.csv")
    public void testSHA256HashCalculation(String expected, String fileName) {
        try {
            FileScanner scanner = new FileScanner();
            Path path = Path.of(TEST_SOURCES_DIR, fileName);
            File file = new File(path.toUri());
            List<Path> files;
            if (file.isFile()){
                files = Collections.singletonList(path);
            } else {
                files = scanner.scanFiles(Path.of(TEST_SOURCES_DIR, fileName));
            }
            String actual = fileHashCalculator.calculateFromFiles(files);
            Assertions.assertEquals(expected, actual);
        } catch (Exception e) {
            Assertions.fail(e.getMessage());
        }

    }
}
